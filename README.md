# Projecte Web Flautilla Hero

### Autors

- Sergio Aroca 
- Marc Contreras
- Miki Jaworski

### Descripció

Repositori del projecte **Flautilla Hero**.

### INDEX

1. [Animació de les notes](https://gitlab.com/mjaworski8/projecte-web-flautilla-hero/wikis/Animació-de-les-notes)
2. [Assignar so a les notes](https://gitlab.com/mjaworski8/projecte-web-flautilla-hero/wikis/Assignar-so-a-les-notes)
3. [Taulells kanban](https://gitlab.com/mjaworski8/projecte-web-flautilla-hero/wikis/TAULELLS-KANBAN)
4. [Temporització d'esprints](https://gitlab.com/mjaworski8/projecte-web-flautilla-hero/wikis/Temporitzaci%C3%B3-d'esprints)
5. [Trello link](https://gitlab.com/mjaworski8/projecte-web-flautilla-hero/wikis/Trello-link)
6. [Gitlab](https://gitlab.com/mjaworski8/projecte-web-flautilla-hero/wikis/gitlab)
7. [Mostrar animació](https://gitlab.com/mjaworski8/projecte-web-flautilla-hero/wikis/mostrar-animaci%C3%B3)
8. [Penjar codi del projecte al repositori](https://gitlab.com/mjaworski8/projecte-web-flautilla-hero/wikis/penjar-codi-del-projecte-al-repositori)
9. [Permetre tocar notes amb ratolí i teclat](https://gitlab.com/mjaworski8/projecte-web-flautilla-hero/wikis/permetre-tocar-notes-amb-ratol%C3%AD-i-teclat)
10. [Raons del projecte](https://gitlab.com/mjaworski8/projecte-web-flautilla-hero/wikis/raons-del-projecte)
11. [Disseny de la interfície web](https://gitlab.com/mjaworski8/projecte-web-flautilla-hero/wikis/disseny-de-l'instrument-html/Disseny-de-la-interf%C3%ADcie-web)
12. [Creació i implementació de les cançons i la puntuació](https://gitlab.com/mjaworski8/projecte-web-flautilla-hero/wikis/creaci%C3%B3-i-implementaci%C3%B3-de-les-can%C3%A7ons-i-la-puntuaci%C3%B3)
13. [Creació sass, web responsive i mediaquerys](https://gitlab.com/mjaworski8/projecte-web-flautilla-hero/wikis/creaci%C3%B3-sass,-web-responsive-i-mediaquerys)
14. [Proposta proves caixa blanca i caixa negre](https://gitlab.com/mjaworski8/projecte-web-flautilla-hero/wikis/proposta%20caixa%20negre%20i%20blanca)
15. [Diari d'errors](https://gitlab.com/mjaworski8/projecte-web-flautilla-hero/wikis/diari-d'errors)
16. [Diari de Tasques](https://gitlab.com/mjaworski8/projecte-web-flautilla-hero/wikis/Diari-de-tasques)


### MANUAL DE JOC

1. Triar canço del menú d'abaix.
2. En el panell central de color blanc sortiran les notes a clickar.
3. Premer les notes per l'ordre del panell de color blanc.

### Puntuació:

100 punts nota correcte.

-50 punts nota incorrecte.

Si la puntuació està a 0 no baixarà negativament.

### BOTONS PER JUGAR

1. A = DO
2. S = RE
3. D = MI
4. F = FA
5. J = SOL
6. K = LA
7. L = SI

