var autoplay = false;
var count = 0;
var notaActual = 0;
var cançoSeleccionada = false;
var cançoPerTocar = "";
var partituraNotes = ["do", "re", "re", "sol", "mi", "fa", "sol", "la", "si", "fa", "la"];
var HappyBirthDay = ["re", "re", "mi", "re", "sol", "fa", "re", "re", "mi", "re", "la", "sol", "re", "re", "re", "si", "sol", "fa", "mi", "do", "do", "si", "sol", "la", "sol"]
var LosSimpson = ["do", "fa", "sol", "do", "mi", "fa", "do", "sol", "mi", "do", "la", "fa", "fa", "fa", "sol", "fa", "fa", "fa", "sol", "si", "si", "do", "mi", "sol", "la", "sol", "mi", "do", "la", "fa", "fa", "fa", "sol", "fa", "fa", "fa", "sol", "si", "do", "do", "do", "do"]
var SuperMarioBros = ["do", "do", "re", "si", "fa", "fa", "fa", "mi", "re", "mi", "mi", "mi", "do"]; // mida original
// var SuperMarioBros = ["do", "do", "re", "si", "fa", "fa", "fa", "mi", "re", "mi", "mi", "mi", "do", "do", "do", "re", "si", "fa", "fa", "fa", "mi", "re", "mi", "mi", "mi", "do"]; // mida * 2
var puntuacio = 0;

document.getElementById("puntuacioValor").innerHTML = puntuacio

var nota = {
    'a': 'do',
    'do': 'C4',
    's': 're',
    're': 'D4',
    'd': 'mi',
    'mi': 'E4',
    'f': 'fa',
    'fa': 'F4',
    'j': 'sol',
    'sol': 'G4',
    'k': 'la',
    'la': 'A4',
    'l': 'si',
    'si': 'B4',
}
// TODO posa notes de les cançons
// ex.
// var notesTitanic = ["do", "re", mi, "fa", "sol", "la", "si"]
function playSound(n) {
    // var delay = new Pizzicato.Effects.Delay({
    //     feedback: 0.6,
    //     time: 0.4,
    //     mix: 0.5
    // });
    // var sineWave = new Pizzicato.Sound({ 
    //     source: 'wave', 
    //     options: {
    //         frequency: 262,
    //         addEfect:delay,
    //     }
    // });

    // sineWave.play();
    // var sound = new Howl({
    //     urls: ['sounds/do.mp3','sounds/re.mp3']
    //   }).play();
    var synth = new Tone.Synth().toMaster();

    /*
    synth.triggerAttackRelease('D4', 0.5, 0);
    synth.triggerAttackRelease('D4', 0.5, 1);
    synth.triggerAttackRelease('E4', 0.5, 2);
    synth.triggerAttackRelease('D4', 0.5, 3);
    synth.triggerAttackRelease('G4',0.5, 4);
    synth.triggerAttackRelease('E4', 1, 5);
    synth.triggerAttackRelease('D4', 0.5, 7);
    synth.triggerAttackRelease('D4', 0.5, 8);
    synth.triggerAttackRelease('E4', 0.5, 9);
    synth.triggerAttackRelease('D4', 0.5, 10);
    synth.triggerAttackRelease('A4', 0.5, 11);
    synth.triggerAttackRelease('G4', 1, 12);
    synth.triggerAttackRelease('D4', 0.5, 14);
    synth.triggerAttackRelease('D4', 0.5, 15);
    synth.triggerAttackRelease('A4', 0.5, 16);//2 més
    synth.triggerAttackRelease('A4', 0.5, 17);
    synth.triggerAttackRelease('G4', 0.5, 18);
    synth.triggerAttackRelease('F4', 0.5, 19);
    synth.triggerAttackRelease('E4', 0.5, 20);
    synth.triggerAttackRelease('A4', 0.5, 21);//1 més
    synth.triggerAttackRelease('A4', 0.5, 22);//1 més
    synth.triggerAttackRelease('A4', 0.5, 23);
    synth.triggerAttackRelease('G4', 0.5, 24);
    synth.triggerAttackRelease('A4', 0.5, 25);
    synth.triggerAttackRelease('G4', 1, 26);
    */
            
    /*synth.triggerAttackRelease('re', 0.5, 0);
    synth.triggerAttackRelease('re', 0.5, 1);
    synth.triggerAttackRelease('mi', 0.5, 2);
    synth.triggerAttackRelease('re', 0.5, 3);
    synth.triggerAttackRelease('sol', 0.5, 4);
    synth.triggerAttackRelease('fa', 1, 5);
    synth.triggerAttackRelease('re', 0.5, 7);
    synth.triggerAttackRelease('re', 0.5, 8);
    synth.triggerAttackRelease('mi', 0.5, 9);
    synth.triggerAttackRelease('re', 0.5, 10);
    synth.triggerAttackRelease('la', 0.5, 11);
    synth.triggerAttackRelease('sol', 1, 12);
    synth.triggerAttackRelease('re', 0.5, 14);
    synth.triggerAttackRelease('re', 0.5, 15);
    synth.triggerAttackRelease('re', 0.5, 16);//2 més
    synth.triggerAttackRelease('si', 0.5, 17);
    synth.triggerAttackRelease('sol', 0.5, 18);
    synth.triggerAttackRelease('fa', 0.5, 19);
    synth.triggerAttackRelease('mi', 0.5, 20);
    synth.triggerAttackRelease('do', 0.5, 21);//1 més
    synth.triggerAttackRelease('do', 0.5, 22);//1 més
    synth.triggerAttackRelease('si', 0.5, 23);
    synth.triggerAttackRelease('sol', 0.5, 24);
    synth.triggerAttackRelease('la', 0.5, 25);
    synth.triggerAttackRelease('sol', 1, 26);   */

    var sound = nota[n];
    synth.triggerAttackRelease(sound, "8n");
    console.log("nota: " + n)
    //sound.play();
    // creix(n);


}


// on load page
function playerName() {
    var playerName = prompt("Qui jugarà")

    while (playerName.length > 20 ||playerName.length < 5) {
        playerName = prompt("Torna a entra l'usuari")
    }

    document.getElementById("playerName").innerHTML = playerName.toUpperCase()
    // posar a html el valor de la variable
}

function notaATocar(notes) {
    if (HappyBirthDay.length == notaActual) {
        document.getElementById("seguentNota").innerHTML = "S'ha acabat la cançó";
        alert("Puntuació obtinguda: " + puntuacio)
        puntuacio = 0
    } else {
        document.getElementById("seguentNota").innerHTML = notes;
    }
}

function sumaPuntuacio(punts) {
    puntuacio += punts
    document.getElementById("puntuacioValor").innerHTML = puntuacio;
}

function restarPuntuacio(punts) {
    puntuacio -= punts
    document.getElementById("puntuacioValor").innerHTML = puntuacio;
}

function keyToCharcode(evt, el) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.key) ? evt.key : evt.keyCode;
    tocaNota(charCode);
}

function tocaNota(charCode) {
    console.log("puntuacio: " + puntuacio)
    if (nota.hasOwnProperty(charCode)) {
        if (!cançoSeleccionada) {
            playSound(nota[charCode]);
        }
        else {
            if (nota[charCode] == cançoPerTocar[notaActual]) {
                sumaPuntuacio(100);
                playSound(nota[charCode]);
                notaActual++;
                notaATocar(cançoPerTocar[notaActual]);

                if (cançoPerTocar.length == notaActual) {
                    cançoSeleccionada = false;
                    document.getElementById("idle").src="white-bg.png";
                }else{
                    document.getElementById("idle").src="checked.png";
                }


            } else {//ha de continuar amb la següent nota o no avança fins que premi la correcte
                //fer animació visual i sonora d'error
                document.getElementById("idle").src="unchecked.png";
                if (puntuacio > 0) {
                    restarPuntuacio(50);
                }
                var sound = new Audio("errorAudio.wav");
                sound.play();
                stateChange(nota[charCode]);
            }

        }

    }
}
function auto() {
    count++;
    if (count % 2 != 0) {
        autoplay = true;
    } else {
        autoplay = false;
        count = 0;
    }
}

function stateChange(newState) {
    setTimeout(function () {
        document.getElementById(newState)
    }, 1000);
}
document.addEventListener('keypress', function (e) {
    keyToCharcode(e, this);
});

document.getElementById("song1").addEventListener("click", function () {

    cançoSeleccionada = true;
    notaActual = 0
    cançoPerTocar = LosSimpson;
    if (autoplay) {
        funcionarAutoplay();
    }
    notaATocar(LosSimpson[0]);
});

document.getElementById("song2").addEventListener("click", function () {
    cançoSeleccionada = true;
    notaActual = 0
    cançoPerTocar = HappyBirthDay;
    if (autoplay) {
        funcionarAutoplay();
    }
    notaATocar(HappyBirthDay[0]);
});

document.getElementById("song3").addEventListener("click", function () {
    cançoSeleccionada = true;
    notaActual = 0
    cançoPerTocar = SuperMarioBros;
    if (autoplay) {
        funcionarAutoplay();
    }
    notaATocar(SuperMarioBros[0]);
});